#ifndef COMPUTERS_H
#define COMPUTERS_H

#include <string>

class Computers
{
    private:
        std::string name;
        std::string type;
        bool built;
        int buildYear;
        int compId;

    public:
        Computers();
        std::string getType();
        std::string getName();
        bool getBuilt();
        int getCompId();
        int getBuildYear();
        void setBuilt(bool b);
        void setType(std::string b);
        void setName(std::string b);
        void setCompId(int b);
        void setBuildYear(int b);
};


#endif // COMPUTERS_H
