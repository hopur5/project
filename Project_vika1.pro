#-------------------------------------------------
#
# Project created by QtCreator 2014-11-28T13:24:50
#
#-------------------------------------------------

QT       += core sql

QT       -= gui

TARGET = Project_vika1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    consoleui.cpp \
    personservice.cpp \
    person.cpp \
    computers.cpp \
    linker.cpp \
    personRepository.cpp \
    computerrepository.cpp \
    linkrepository.cpp

HEADERS += \
    consoleui.h \
    personservice.h \
    person.h \
    computers.h \
    linker.h \
    personRepository.h \
    computerrepository.h \
    linkrepository.h
