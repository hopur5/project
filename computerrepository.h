#ifndef COMPUTERREPOSITORY_H
#define COMPUTERREPOSITORY_H

#include "computers.h"
#include "linker.h"
#include <vector>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtDebug>

class computerRepository
{
    public:
        computerRepository();
        QSqlDatabase databaseConnect();
        Computers computersToVector(Computers c, QSqlQuery q);
        std::vector<Computers> computersFromDatabase(int sortChoice2);
        std::vector<Computers> searchComputer(std::string str);
        std::vector<Computers> searchComputerBuildYear(std::string str);
        std::vector<Computers> getTableRelation(int scientistID);
        void addComputer(Computers c);
};

#endif // COMPUTERREPOSITORY_H
