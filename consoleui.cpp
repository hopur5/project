#include "consoleui.h"
#include <iomanip>
#include <limits>

consoleUI::consoleUI()
{
    perServ = personService();
}
void consoleUI::start()// The function that starts the program
{
    //Opening welcome message
    std::string line;
    std::ifstream inFile ("Menu.txt");

    while(getline(inFile, line))
    {
        std::cout << line << std::endl;
    }
    std::cout << "\nThe ULTIMATE database of famous Computer Scientists and famous Computers" << std::endl;
    do
    {
        int menuChoice = 0;
        do
        {
            std::cout << "\n1. Add" << std::setw(15) << "2. Print" << std::setw(15) << "3. Search" << std::setw(15)  << "4. Quit\n";
            std::cout << "\nChoice: ";
            std::cin >> menuChoice;
            while(std::cin.fail() || !std::cin >> menuChoice)
            {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
        while(menuChoice < 1 || menuChoice > 4);

        std::cout << std::endl;
        std::cin.ignore(10000, '\n');
        std::string str;
        Person p;
        Computers c;

        switch(menuChoice)
        {

            case 1:

                //Adding a Person to database
                std::cout << "What would you like to add? \n(1) Scientist \n(2) Computer \n(3) Connection between a scientist and a computer\n";
                int addChoice;
                std::cin >> addChoice;
                if(addChoice == 1)
                {
                    p = getPersonInfo();
                    perServ.addPerson(p);
                }
                else if(addChoice == 2)
                {
                    c = getComputerInfo();
                    perServ.addComputer(c);
                }
                else if(addChoice == 3)
                {
                    addConnection();        //Adds a connection between a scientist
                }                           //and a computer in the database.
                break;

            case 2:

                //Print from database
                std::cout << "What would you like to print? \n";
                std::cout << "(1) Scientists \n(2) Computers\n";
                int printChoice;
                std::cin >> printChoice;

                if(printChoice == 1)
                {
                    printScientists();
                }
                else if(printChoice == 2)
                {
                    printComputers();
                }
                break;

            case 3:

                //Search from database
                std::cout << "What would you like to search for?\n";
                std::cout << "(1) Scientist \n(2) Computer\n";
                int searchChoice;

                std::cin >> searchChoice;

                if(searchChoice == 1)
                {
                    searchScientist();
                }
                else if(searchChoice == 2)
                {
                    searchComputer();
                }
                break;

            case 4:

                exit(0);
                break;

            default:

                std::cout << "\nInvalid choice\n";
                break;
        }

    }while(true);
}
void consoleUI::printLinkToComputer()//Print the scientists that are linked to a computer
{
    std::string scientistName = "";
    std::vector<Person> list;
    int computerID;
    int choice;

    std::cout << "\nWould you like to print the scientists that worked on this computer? \n(1) Yes \n(2) No\n";
    std::cin >> choice;
    if(choice == 1)
    {
        computerID = computerVector.at(0).getCompId();
        scientistVector = perServ.getTableRelation(computerID);
        if(scientistVector.size() == 0)
        {
            std::cout << "\nNo scientists are linked to this computer\n";
        }
        printList(scientistVector);
    }
    else
    {
        std::cout << "\nGoing back to main menu" << std::endl;
    }
}

void consoleUI::printLinkToScientist() // Prints the computers that are linked to a scientist
{
    std::string computerName = "";
    int scientistID;
    int choice;

    std::cout << "\nWould you like to print the computers this scientist worked on? \n(1) Yes \n(2) No\n";
    std::cin >> choice;
    if(choice == 1)
    {
        scientistID = scientistVector.at(0).getID();

        computerVector = perServ.getTableRelationC(scientistID);
        if(computerVector.size() == 0)
        {
            std::cout << "\nNo computers are linked to this scientist\n";
        }
        printComputers(computerVector);
    }
}

void consoleUI::linkFromSearch() // Links a scientists to computer after you have searched for a scientists
{
    Linker l;
    std::string scientistName = "";
    std::string str;
    char answer;
    int scientistID;
    int computerID;
    int choice;

    std::cout << "\nWould you like to link the scientist to a computer? \n(1) Yes \n(2) No\n";
    std::cin >> choice;

    if(choice == 1)
    {
        do
        {
            scientistID = scientistVector.at(0).getID();
            scientistName = scientistVector.at(0).getName();
            std::cout << "\nNow search through the computer database to find one to link to\n";
            std::cout << "Enter name to search for: ";
            std::cin.ignore(10000, '\n');
            getline(std::cin, str);
            str = perServ.trim(str);
            computerVector = perServ.searchComputer(str);
            printComputers(computerVector);
            if(computerVector.size() == 0)
            {
                std::cout << "\nNo computer was found with that name, would you like to try again? (Y/any other key) ";
                std::cin >> answer;
            }
            else if(computerVector.size() > 1)
            {
                std::cout << "Would you like to keep searching? (Y/any other key) ";
                std::cin >> answer;
            }
        }
        while((computerVector.size() != 1) && (answer == 'Y'));

        if (computerVector.size() == 1)
        {
            computerID =  computerVector.at(0).getCompId();
            std::cout << "\nWould you like to link " << scientistName << " to " << computerVector.at(0).getName() << std::endl;
            std::cout << "(1) Yes \n(2) No\n";
            std::cin >> choice;

            if(choice == 1)
            {
                l.setSid(scientistID);
                l.setCid(computerID);
                perServ.addLink(l);
            }
        }
    }
}


void consoleUI::printComputerId(std::vector<Computers> l)// Prints the computer ID
{
    for(unsigned int i = 0; i < l.size(); i++)
    {
        std::string nameTemp = l.at(i).getName();
        int idTemp = l.at(i).getCompId();

        std::cout << std::setw(3) << nameTemp << ", "  << "ID: " << idTemp << std::endl;
    }
}

void consoleUI::printScientistId(std::vector<Person> l)// Prints the scientist ID
{
    for(unsigned int i = 0; i < l.size(); i++)
    {
        std::string nameTemp = l.at(i).getName();
        int idTemp = l.at(i).getID();

        std::cout << std::setw(3) << nameTemp << ", "  << "ID: " << idTemp << std::endl;
    }
}

void consoleUI::printList(std::vector<Person> l)// Prints the person vector that we got from the database
{
    for(unsigned int i = 0; i < l.size(); i++)
    {
        int idTemp = l.at(i).getID();
        int birthYearTemp = l.at(i).getBirthYear();
        int deathYearTemp = l.at(i).getDeathYear();
        std::string genderTemp = l.at(i).getGender();
        std::string nameTemp = l.at(i).getName();

        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::setw(3) << i + 1<<"." << std::setw(16) << "Name:" << std::setw(21) << nameTemp << std::endl;
        std::cout << std::setw(20) << "Birth year:" << std::setw(21) << birthYearTemp << std::endl;
        std::cout <<  std::setw(20) << "Death year:"<< std::setw(21) << deathYearTemp << std::endl;
        std::cout << std::setw(20) << "Gender:"<< std::setw(21) << genderTemp << std::setw(15) << std::endl;
        std::cout << std::setw(20) << "ID:"<< std::setw(21) << idTemp << std::setw(15) << std::endl;
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::endl;
    }
}

void consoleUI::printListC(std::vector<Computers> l)// Prints the computer vector that we got from the database (through the logical layer)
{
    for(unsigned int i = 0; i < l.size(); i++)
    {
        bool builtTemp = l.at(i).getBuilt();
        int idTemp = l.at(i).getCompId();
        int buildYearTemp = l.at(i).getBuildYear();
        std::string nameTemp = l.at(i).getName();
        std::string typeTemp = l.at(i).getType();

        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::setw(3) << i + 1 <<"." << std::setw(16) << "Name:" << std::setw(21) << nameTemp << std::endl;
        std::cout << std::setw(20) << "Computer type" << std::setw(21) << typeTemp << std::endl;
        std::cout <<  std::setw(20) << "Build year:"<< std::setw(21) << buildYearTemp << std::endl;
        std::cout << std::setw(20) << "Computer built:"<< std::setw(21) << builtTemp << std::setw(15) << std::endl;
        std::cout << std::setw(20) << "ID:"<< std::setw(21) << idTemp << std::setw(15) << std::endl;
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::endl;
    }
}

void consoleUI::printComputers(std::vector<Computers> l)// Prints the computer vector that we got from the database (through the logical layer)
{
    for(unsigned int i = 0; i < l.size(); i++)
    {
        int buildYearTemp = l.at(i).getBuildYear();
        int compIdTemp = l.at(i).getCompId();
        std::string typeTemp = l.at(i).getType();
        std::string nameTemp = l.at(i).getName();
        std::string built = "No";

        if(l.at(i).getBuilt())
        {
            built = "Yes";
        }
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::setw(3) << i + 1<<"." << std::setw(16) << "Name:" << std::setw(21) << nameTemp << std::endl;
        std::cout << std::setw(20) << "Build year:" << std::setw(21) << buildYearTemp << std::endl;
        std::cout <<  std::setw(20) << "Type:"<< std::setw(21) << typeTemp << std::endl;
        std::cout <<  std::setw(20) << "Built:"<< std::setw(21) << built << std::endl;
        std::cout << std::setw(20) << "ID:"<< std::setw(21) << compIdTemp << std::setw(15) << std::endl;
        std::cout << "---------------------------------------------------" << std::endl;
        std::cout << std::endl;
    }
}

Linker consoleUI::getLinkInfo()// Gets the ID's from scientists and computers that will link
{
    Linker l;
    int sIdTemp;
    int cIdTemp;

    std::cout << "\nScientist ID to connect: ";
    std::cin >> sIdTemp;
    std::cout << "Computer ID to connect to: ";
    std::cin >> cIdTemp;
    l.setSid(sIdTemp);
    l.setCid(cIdTemp);

    return l;
}

Computers consoleUI::getComputerInfo()//Gets all the info for the computer to put into the database
{
    Computers c;
    std::string nameTemp;
    int typeChoice;
    int buildYearTemp;
    int builtTemp;

    std::cout << "\nName: ";
    std::cin.ignore(1000, '\n');
    std::getline(std::cin, nameTemp);
    c.setName(nameTemp);

    do
    {
        std::cout << "\nType: \n(1) Mechanical \n(2) Electro-Mechanical \n(3) Electronic\n";
        std::cin >> typeChoice;
        while(std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    while(typeChoice < 1 || typeChoice > 3);

    switch(typeChoice)
    {
        case 1:
            c.setType("Mechanical");
            break;
        case 2:
            c.setType("Electro-Mechanical");
            break;
        case 3:
            c.setType("Electronic");
            break;
    }

    while ((std::cout << "\nBuildyear: ")
           && (!(std::cin >> buildYearTemp) || buildYearTemp < 0 || buildYearTemp > 2014))
    {
        std::cout << "\nThat's not a number between 0 and 2014... \n ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    c.setBuildYear(buildYearTemp);

    while ((std::cout << "\nWas the computer built?: \n(1) Yes \n(2) No\n")
           && (!(std::cin >> builtTemp) || builtTemp < 1 || builtTemp > 2))
    {
        std::cout << "\nInvalid choice, please enter 1 or 2\n";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    if(builtTemp == 1)
    {
        c.setBuilt(1);
    }
    else if(builtTemp == 2)
    {
        c.setBuilt(0);
    }

    return c;
}

Person consoleUI::getPersonInfo()// Gets all the info for the scientist to put into the database
{
    Person p;
    std::string nameTemp;
    std::string genderTemp;
    int birthYearTemp;
    int deathYearTemp;

    std::cout << "\nName: ";
    std::cin.ignore(1000, '\n');
    std::getline(std::cin, nameTemp);
    p.setName(nameTemp);

    do
    {
        std::cout << "\nGender(M/F): ";
        std::getline(std::cin, genderTemp);
        while(std::cin.fail())
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }
    while(genderTemp != "M" && genderTemp != "F");

    if(genderTemp == "M")
    {
        p.setGender("Male");
    }

    else if(genderTemp == "F")
    {
        p.setGender("Female");
    }


    while ((std::cout << "\nBirth year (0 - 2014): ")
           && (!(std::cin >> birthYearTemp) || birthYearTemp < 0 || birthYearTemp > 2014))
    {
        std::cout << "\nThat's not a number between 0 and 2014... ";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    p.setBirtYear(birthYearTemp);

    while ((std::cout << "\nYear of death (0 if the person is still alive): ")
           && (!(std::cin >> deathYearTemp) || ((deathYearTemp < birthYearTemp) && deathYearTemp != 0) || deathYearTemp > 2014))
    {
        std::cout << "\nThats not a number between the persons birth and today...\n";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    p.setDeathYear(deathYearTemp);

    return p;
}

void consoleUI::addConnection()// Adds a connection between a scientist and a computer
{

    Linker l;
    int exit = 0;

    do{
        std::cout << "\nPlease enter ID of a scientist and then the ID of a computer to connect them in the database.\n";
        std::cout << "(1) View scientists and theyre ID's \n(2) View computers and theyre ID's \n(3) Link ID's \n(4) Exit to main menu\n";
        int linkChoice;
        while ((std::cout << "\nEnter choice : ")
               && (!(std::cin >> linkChoice) || ((linkChoice < 1) || (linkChoice > 4))))

        {
            std::cout << "\nPlease enter a number between 1 and 4\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        switch(linkChoice)
        {
        case 1:
            std::cout << std::endl;
            scientistVector = perServ.getScientistData(5); //Gets list and sorts by ID by default
            printScientistId(scientistVector);
            break;
        case 2:
            std::cout << std::endl;
            computerVector = perServ.getComputerData(4); //Gets list and sorts by ID by default
            printComputerId(computerVector);
            break;
        case 3:
            l = getLinkInfo();
            perServ.addLink(l);
            std::cout << "\nA link has been added.\n";
            exit = 1;
            break;
        case 4:
            exit = 1;
            break;
        }
    }
    while(exit == 0);
}

void consoleUI::printScientists()// Asks the user how the scientist will be sorted and prints them out
{
    int sortChoice;

    while ((std::cout << "\nHow would you like the results sorted? \n(1) Name \n(2) Gender \n(3) Birth year \n(4) Year of death \n(5) ID\n")
           && (!(std::cin >> sortChoice) || ((sortChoice < 1) || (sortChoice > 5))))
    {
        std::cout << "\nThats not a number between 1 and 5...\n";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    scientistVector = perServ.getScientistData(sortChoice);
    printList(scientistVector);
}

void consoleUI::printComputers()// Asks the user how the computers will be sorted and prints them out
{
    int sortChoice2;

    while ((std::cout << "\nHow would you like the results sorted? \n(1) Name \n(2) Build year \n(3) Type \n(4) ID\n")
           && (!(std::cin >> sortChoice2) || ((sortChoice2 < 1) || (sortChoice2 > 4))))
    {
        std::cout << "\nThats not a number between 1 and 4...\n";
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    computerVector = perServ.getComputerData(sortChoice2);
    printComputers(computerVector);
}

void consoleUI::searchScientist()// Searches for a scientists by name, birth year or death year
{
    std::string searchName;
    int val;

    std::cout << "\nWhat would you like to search for?\n";
    std::cout << "(1) Name \n(2) Year\n";
    std::cin >> val;

    if(val == 1)
    {
        std::cout << "\nIf you narrow the search down to one scientist you can link him to a computer\n";
        std::cout << "Enter name to search for: ";
        std::cin.ignore(10000, '\n');
        getline(std::cin, searchName);
        searchName = perServ.trim(searchName);
        scientistVector = perServ.searchPerson(searchName);
        printList(scientistVector);
        if(scientistVector.size() == 0)
        {
            std::cout << "\nSorry, there is no scientist with that name in the database\n";
        }
        if(scientistVector.size() == 1)
        {
            printLinkToScientist();

            linkFromSearch();
        }
    }
    else if (val == 2)
    {
        std::cout << "What would you like to search for?\n";
        std::cout << "(1) Birth year \n(2) Death year\n";
        int choice;
        std::cin >> choice;
        if(choice == 1)
        {
            std::cout << "Enter birth year to search for: ";
            std::string birthyear;
            std::cin.ignore(10000, '\n');
            getline(std::cin, birthyear);
            birthyear = perServ.trim(birthyear);
            scientistVector = perServ.searchPersonBirthYear(birthyear);
            printList(scientistVector);
            if(scientistVector.size() == 0)
            {
                std::cout << "\nSorry, there is no scientist with that birth year in the database\n";
            }
        }
        else if(choice == 2)
        {
            std::cout << "Enter death year to search for: ";
            std::string deathyear;
            std::cin.ignore(10000, '\n');
            getline(std::cin, deathyear);
            deathyear = perServ.trim(deathyear);
            scientistVector = perServ.searchPersonDeathYear(deathyear);
            printList(scientistVector);
            if(scientistVector.size() == 0)
            {
                std::cout << "\nSorry, there is no scientist with that death year in the database\n";
            }
        }
    }
}

void consoleUI::searchComputer()// Searches for a computer by name or build year
{
    std::string searchName;
    int choice;

    std::cout << "\nWhat would you like to search for?\n";
    std::cout << "(1) Name \n(2) Build year\n";
    std::cin >> choice;

    if(choice == 1)
    {
        std::cout << "Enter name to search for: ";
        std::cin.ignore(10000, '\n');
        getline(std::cin, searchName);
        searchName = perServ.trim(searchName);
        computerVector = perServ.searchComputer(searchName);
        printComputers(computerVector);
        if(computerVector.size() == 0)
        {
            std::cout << "\nSorry, there is no computer with that name in the database\n";
        }
        if(computerVector.size() == 1)      //Gives you the choice to print
        {                                   //Scientists linked to the computer
            printLinkToComputer();          //if search is narrowed down to 1
        }
    }
    else if(choice == 2)
    {
        std::cout << "Enter build year to search for: ";
        std::string buildyear;
        std::cin.ignore(10000, '\n');
        getline(std::cin, buildyear);
        computerVector = perServ.searchComputerBuildYear(buildyear);
        printComputers(computerVector);
        if(computerVector.size() == 0)
        {
            std::cout << "\nSorry, there is no computer built that year, in the database\n";
        }
    }
}
