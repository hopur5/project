#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <iostream>
#include "personservice.h"
#include "computers.h"
#include "linker.h"
#include <vector>
#include <fstream>
#include <string>
#include <iomanip>

class consoleUI
{
    public:
        consoleUI();
        Linker getLinkInfo();
        Computers getComputerInfo();
        Person getPersonInfo();
        std::vector<Person> scientistVector;
        std::vector<Computers> computerVector;
        void start();
        void printLinkToComputer();
        void printLinkToScientist();
        void linkFromSearch();
        void printComputerId(std::vector<Computers> l);
        void printScientistId(std::vector<Person> l);
        void printList(std::vector<Person> l);
        void printListC(std::vector<Computers> l);
        void printComputers(std::vector<Computers> l);
        void printScientists();
        void printComputers();
        void addConnection();
        void searchScientist();
        void searchComputer();

    private:
        personService perServ;
};

#endif // CONSOLEUI_H
