#include "REPOSITORY.h"
#include <fstream>
#include <vector>
#include <string>
#include <QtSql>
#include <QString>
#include <consoleui.h>


REPOSITORY::REPOSITORY()
{
}
QSqlDatabase REPOSITORY::databaseConnect()
<<<<<<< HEAD
{
=======
{    
<<<<<<< HEAD

    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString dbName = "CSINFO.sqlite";
    db.setDatabaseName(dbName);
    QSqlQuery query;
    db.open();
    query.prepare("PRAGMA foreign_keys ON");
    query.exec();
    query.clear();
=======
>>>>>>> eaeb5d8fa77296f68339ed03066ea638ad51a66c
        QString connectionName = "HAL";

        QSqlDatabase db;

        if(QSqlDatabase::contains(connectionName))
        {
            db = QSqlDatabase::database(connectionName);
        }
        else
        {
            db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
            db.setDatabaseName("CSINFO.sqlite");

            db.open();
        }
>>>>>>> 747f1757cb871cd7bb5021bff6a2f9002bc9ac6c

    return db;
}

void REPOSITORY::addComputer(Computers c)
{
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Computers(Name, Buildyear, Type, Built) VALUES(:name, :buildyear, :type, :built)");

        query.bindValue(":name", QString::fromStdString(c.getName()));
        query.bindValue(":buildyear", QString::number(c.getBuildYear()));
        query.bindValue(":type", QString::fromStdString(c.getType()));
        query.bindValue(":built", QString::number(c.getBuilt()));
        query.exec();
    }
    //db.close();
}

void REPOSITORY::add(Person p)
{
    QSqlDatabase db = databaseConnect();
    //QSqlQuery query;
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Scientists(Name, Gender, Birthyear, Deathyear) VALUES(:name, :gender, :birthyear, :deathyear)");
        query.bindValue(":name", QString::fromStdString(p.getName()));
        query.bindValue(":gender", QString::fromStdString(p.getGender()));
        query.bindValue(":birthyear", QString::number(p.getBirthYear()));
        query.bindValue(":deathyear", QString::number(p.getDeathYear()));
        query.exec();
    }
    //db.close();
}

void REPOSITORY::addLink(Linker l)
{
    QSqlDatabase db = databaseConnect();
    //QSqlQuery query;
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Link(s_ID, c_ID) VALUES(:s_id, :c_id)");
        query.bindValue(":s_id", QString::number(l.getSid()));
        query.bindValue(":c_id", QString::number(l.getCid()));
        query.exec();
    }
   // db.close();
}

std::vector<Computers> REPOSITORY::computersFromDatabase(int sortChoice2)
{
    QString searchString;
    switch(sortChoice2){
    case 1:
        searchString = "SELECT * FROM Computers ORDER BY Name ASC";
        break;
    case 2:
        searchString = "SELECT * FROM Computers ORDER BY Buildyear ASC";
        break;
    case 3:
        searchString = "SELECT * FROM Computers ORDER BY Type, Name ASC";
        break;
    case 4:
        searchString = "SELECT * FROM Computers ORDER BY ID";
        break;
    default:
        searchString = "SELECT * FROM Computers";
        break;
    }
    Computers c;
    std::vector<Computers> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare(searchString);
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }
    return list;
}
std::vector<Person> REPOSITORY::scientistsFromDatabase(int sortChoice)
{
    QString searchString;
    switch(sortChoice){
    case 1:
        searchString = "SELECT * FROM Scientists ORDER BY Name ASC";
        break;
    case 2:
        searchString = "SELECT * FROM Scientists ORDER BY Gender, Name ASC";
        break;
    case 3:
        searchString = "SELECT * FROM Scientists ORDER BY Birthyear";
        break;
    case 4:
        searchString = "SELECT * FROM Scientists ORDER BY Deathyear";
        break;
    case 5:
        searchString = "SELECT * FROM Scientists ORDER BY ID";
        break;
    default:
        searchString = "SELECT * FROM Scientists";
        break;
    }

    Person c;
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare(searchString);
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }
    //db.close();
  return list;
}
std::vector<Person> REPOSITORY::searchPerson(std::string str)
{
    Person c;
    QString searchString = QString::fromStdString(str);
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Scientists  WHERE Name LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }
    return list;
}
std::vector<Computers> REPOSITORY::searchComputer(std::string str)
{
    Computers c;
    QString searchString = QString::fromStdString(str);
    std::vector<Computers> list;

    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Computers  WHERE Name LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }
   // db.close();
    return list;
}
Person REPOSITORY::scientistsToVector(Person c, QSqlQuery q)
{
    c.setName(q.value("Name").toString().toStdString());
    c.setBirtYear(q.value("Birthyear").toInt());
    c.setDeathYear(q.value("Deathyear").toInt());
    c.setGender(q.value("Gender").toString().toStdString());
    c.setID(q.value("ID").toInt());

    return c;
}

Computers REPOSITORY::computersToVector(Computers c, QSqlQuery q)
{
    c.setName(q.value(0).toString().toStdString());
    c.setBuildYear(q.value(1).toInt());
    c.setType(q.value(2).toString().toStdString());
    c.setBuilt(q.value(3).toBool());
    c.setCompId(q.value(4).toInt());

    return c;
}





