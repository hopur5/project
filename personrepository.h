#ifndef PERSONREPOSITORY_H
#define PERSONREPOSITORY_H

#include "person.h"
#include "computers.h"
#include "linker.h"
#include <vector>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtDebug>

class personRepository
{
    private:
        std::vector<Person> personVector;

    public:
        personRepository();
        QSqlDatabase databaseConnect();
        Computers computersToVector(Computers c, QSqlQuery q);
        Person scientistsToVector(Person c, QSqlQuery q);
        std::vector<Person> scientistsFromDatabase(int sortChoice);
        std::vector<Person> searchPerson(std::string str);
        std::vector<Person> getTableRelation(int computerID);
        std::vector<Person> searchPersonBirthYear(std::string year);
        std::vector<Person> searchPersonDeathYear(std::string year);
        std::vector<Computers> searchComputer(std::string str);
        std::vector<Computers> computersFromDatabase(int sortChoice2);
        void add(Person p);
        void addComputer(Computers c);
};

#endif // PERSONREPOSITORY_H
