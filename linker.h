#ifndef LINKER_H
#define LINKER_H

class Linker
{
    public:
        Linker();
        int getSid();
        int getCid();
        void setSid(int b);
        void setCid(int b);

    private:
        int s_ID;
        int c_ID;
};

#endif // LINKER_H
