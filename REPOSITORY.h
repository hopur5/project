#ifndef REPOSITORY_H
#define REPOSITORY_H
#include "person.h"
#include "computers.h"
#include "linker.h"
#include <vector>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtDebug>

class REPOSITORY
{
private:
    std::vector<Person> personVector;

public:
    REPOSITORY();
    QSqlDatabase databaseConnect();
    void addComputer(Computers c);
    void add(Person p);
    void addLink(Linker l);
    std::vector<Computers> computersFromDatabase(int sortChoice2);
    std::vector<Person> scientistsFromDatabase(int sortChoice);
    std::vector<Person> searchPerson(std::string str);
    std::vector<Computers> searchComputer(std::string str);
<<<<<<< HEAD
    Person scientistsToVector(Person c, QSqlQuery q);
    Computers computersToVector(Computers c, QSqlQuery q);
=======
    void addComputer(Computers c);
    void addLink(Linker l);
>>>>>>> eaeb5d8fa77296f68339ed03066ea638ad51a66c

};

#endif // REPOSITORY_H
