#include "linker.h"

Linker::Linker()
{
    s_ID = 0;
    c_ID = 0;
}

void Linker::setSid(int b)
{
    s_ID = b;
}

void Linker::setCid(int b)
{
    c_ID = b;
}

int Linker::getSid()
{
    return s_ID;
}

int Linker::getCid()
{
    return c_ID;
}


