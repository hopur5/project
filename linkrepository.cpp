#include "linkrepository.h"

linkRepository::linkRepository()
{

}

QSqlDatabase linkRepository::databaseConnect()// Gets connection to the database
{
    QString connectionName = "HAL";
    QSqlDatabase db;   
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("CSINFO.sqlite");

        db.open();
    }

    return db;
}

void linkRepository::addLink(Linker l) // Adds links by computer id and scientists id
{
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);    
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Link(s_ID, c_ID) VALUES(:s_id, :c_id)");
        query.bindValue(":s_id", QString::number(l.getSid()));
        query.bindValue(":c_id", QString::number(l.getCid()));
        query.exec();
    }
}
