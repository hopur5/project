#ifndef PERSON_H
#define PERSON_H

#include <string>

class Person
{
    public:
        Person();
        std::string getGender();
        std::string getName();
        int getBirthYear();
        int getDeathYear();
        int getID();
        void setBirtYear(int b);
        void setDeathYear(int b);
        void setGender(std::string b);
        void setName(std::string b);
        void setID(int b);

    private:
        std::string name;
        std::string gender;
        int birthYear;
        int deathYear;
        int ID;
};


#endif // PERSON_H
