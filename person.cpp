#include "person.h"

Person::Person()
{
    birthYear = 0;
    deathYear = 0;
    name = "";
    gender = "";
    ID = 0;
}

void Person::setBirtYear(int b)
{
    birthYear = b;
}

void Person::setDeathYear(int b)
{
    deathYear = b;
}

void Person::setGender(std::string b)
{
    gender = b;
}

void Person::setName(std::string b)
{
    name = b;
}

void Person::setID(int b)
{
    ID = b;
}

int Person::getBirthYear()
{
    return birthYear;
}

int Person::getDeathYear()
{
    return deathYear;
}

std::string Person::getGender()
{
    return gender;
}

std::string Person::getName()
{
    return name;
}

int Person::getID()
{
    return ID;
}


