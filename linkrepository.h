#ifndef LINKREPOSITORY_H
#define LINKREPOSITORY_H

#include "linker.h"
#include <vector>
#include <QSql>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QtDebug>

class linkRepository
{
    public:
        linkRepository();
        QSqlDatabase databaseConnect();
        void addLink(Linker l);
};

#endif // LINKREPOSITORY_H
