#include "PERSONREPOSITORY.h"
#include <fstream>
#include <vector>
#include <string>
#include <QtSql>
#include <QString>
#include <consoleui.h>

personRepository::personRepository()
{

}

QSqlDatabase personRepository::databaseConnect()// Gets connection to the database
{    
    QString connectionName = "HAL";

    QSqlDatabase db;

    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("CSINFO.sqlite");

        db.open();
    }

    return db;
}

void personRepository::add(Person p)// Adds a scientists to the database
{
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Scientists(Name, Gender, Birthyear, Deathyear) VALUES(:name, :gender, :birthyear, :deathyear)");
        query.bindValue(":name", QString::fromStdString(p.getName()));
        query.bindValue(":gender", QString::fromStdString(p.getGender()));
        query.bindValue(":birthyear", QString::number(p.getBirthYear()));
        query.bindValue(":deathyear", QString::number(p.getDeathYear()));
        query.exec();
    }
}

std::vector<Person> personRepository::scientistsFromDatabase(int sortChoice)// Sorts the scientists by user choice and puts them into a vector
{
    QString searchString;
    switch(sortChoice){
    case 1:
        searchString = "SELECT * FROM Scientists ORDER BY Name ASC";
        break;
    case 2:
        searchString = "SELECT * FROM Scientists ORDER BY Gender, Name ASC";
        break;
    case 3:
        searchString = "SELECT * FROM Scientists ORDER BY Birthyear";
        break;
    case 4:
        searchString = "SELECT * FROM Scientists ORDER BY Deathyear";
        break;
    case 5:
        searchString = "SELECT * FROM Scientists ORDER BY ID";
        break;
    default:
        searchString = "SELECT * FROM Scientists";
        break;
    }

    Person c;
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare(searchString);
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

std::vector<Person> personRepository::getTableRelation(int computerID)// Gets all the scientists that worked on/are linked to this computer ID
{
    Person c;
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    QString ID = QString::number(computerID);
    if(db.isOpen())
    {
        query.prepare("SELECT s.Name, s.Gender, s.Birthyear, s.Deathyear, s.ID FROM Scientists s INNER JOIN Link l ON l.s_ID = s.ID INNER JOIN Computers c ON l.c_ID = c.ID WHERE c.ID = " + ID);
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

std::vector<Person> personRepository::searchPerson(std::string str)//Searches for a person in the database
{
    Person c;
    QString searchString = QString::fromStdString(str);
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Scientists  WHERE Name LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

Person personRepository::scientistsToVector(Person c, QSqlQuery q)// Sets database info to vector info
{
    c.setName(q.value("Name").toString().toStdString());
    c.setBirtYear(q.value("Birthyear").toInt());
    c.setDeathYear(q.value("Deathyear").toInt());
    c.setGender(q.value("Gender").toString().toStdString());
    c.setID(q.value("ID").toInt());

    return c;
}

std::vector<Person> personRepository::searchPersonBirthYear(std::string year)// Searches for scientist's birth year
{
    Person c;
    QString searchString = QString::fromStdString(year);
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Scientists  WHERE Birthyear LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

std::vector<Person> personRepository::searchPersonDeathYear(std::string year)// Searches for scientist's death year
{
    Person c;
    QString searchString = QString::fromStdString(year);
    std::vector<Person> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Scientists  WHERE Deathyear LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = scientistsToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}


