#include "computers.h"

Computers::Computers()
{
    buildYear = 0;
    compId = 0;
    name = "";
    type = "";
    built = false;
}

void Computers::setBuildYear(int b)
{
    buildYear = b;
}

void Computers::setCompId(int b)
{
    compId = b;
}

void Computers::setName(std::string b)
{
    name = b;
}

void Computers::setType(std::string b)
{
    type = b;
}

void Computers::setBuilt(bool b)
{
    built = b;
}

int Computers::getBuildYear()
{
    return buildYear;
}

int Computers::getCompId()
{
    return compId;
}

std::string Computers::getName()
{
    return name;
}

std::string Computers::getType()
{
    return type;
}

bool Computers::getBuilt()
{
    return built;
}
