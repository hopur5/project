#include "COMPUTERREPOSITORY.h"
#include <fstream>
#include <vector>
#include <string>
#include <QtSql>
#include <QString>
#include <consoleui.h>


computerRepository::computerRepository()
{

}

QSqlDatabase computerRepository::databaseConnect()
{
    QString connectionName = "HAL";
    QSqlDatabase db;
    if(QSqlDatabase::contains(connectionName))
    {
        db = QSqlDatabase::database(connectionName);
    }
    else
    {
        db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
        db.setDatabaseName("CSINFO.sqlite");
        db.open();
    }

    return db;
}

void computerRepository::addComputer(Computers c)// Adds a computer to the database
{
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("INSERT INTO Computers(Name, Buildyear, Type, Built) VALUES(:name, :buildyear, :type, :built)");

        query.bindValue(":name", QString::fromStdString(c.getName()));
        query.bindValue(":buildyear", QString::number(c.getBuildYear()));
        query.bindValue(":type", QString::fromStdString(c.getType()));
        query.bindValue(":built", QString::number(c.getBuilt()));
        query.exec();
    }
}

std::vector<Computers> computerRepository::computersFromDatabase(int sortChoice2)// Sorts the computers by user choice and puts them into a vector
{
    QString searchString;
    switch(sortChoice2){
    case 1:
        searchString = "SELECT * FROM Computers ORDER BY Name ASC";
        break;
    case 2:
        searchString = "SELECT * FROM Computers ORDER BY Buildyear ASC";
        break;
    case 3:
        searchString = "SELECT * FROM Computers ORDER BY Type, Name ASC";
        break;
    case 4:
        searchString = "SELECT * FROM Computers ORDER BY ID";
        break;
    default:
        searchString = "SELECT * FROM Computers";
        break;
    }
    Computers c;
    std::vector<Computers> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare(searchString);
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

std::vector<Computers> computerRepository::searchComputer(std::string str)// Search for a computer by name
{
    Computers c;
    QString searchString = QString::fromStdString(str);
    std::vector<Computers> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Computers  WHERE Name LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

Computers computerRepository::computersToVector(Computers c, QSqlQuery q)// Sets database info to vector info
{
    c.setName(q.value("Name").toString().toStdString());
    c.setBuildYear(q.value("Buildyear").toInt());
    c.setType(q.value("Type").toString().toStdString());
    c.setBuilt(q.value("Built").toBool());
    c.setCompId(q.value("ID").toInt());

    return c;
}

std::vector<Computers> computerRepository::searchComputerBuildYear(std::string str)// Search for a computer after build year
{
    Computers c;
    QString searchString = QString::fromStdString(str);
    std::vector<Computers> list;
    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    if(db.isOpen())
    {
        query.prepare("SELECT * FROM Computers  WHERE Buildyear LIKE \"%" + searchString + "%\"" );
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}

std::vector<Computers> computerRepository::getTableRelation(int scientistID) // Gets all the computers linked to/worked on by this scientist ID
{
    Computers c;
    std::vector<Computers> list;

    QSqlDatabase db = databaseConnect();
    QSqlQuery query(db);
    QString ID = QString::number(scientistID);
    if(db.isOpen())
    {
        query.prepare("SELECT c.Name, c.Buildyear, c.Type, c.Built, c.ID FROM Computers c INNER JOIN Link l ON l.c_ID = c.ID INNER JOIN Scientists s ON l.s_ID = s.ID WHERE s.ID = " + ID);
        query.exec();
        while(query.next())
        {
            c = computersToVector(c, query);
            list.push_back(c);
        }
    }

    return list;
}
