#ifndef PERSONSERVICE_H
#define PERSONSERVICE_H

#include "person.h"
#include "personRepository.h"
#include "computerrepository.h"
#include "linkrepository.h"
#include "computers.h"
#include <vector>
#include <algorithm>

class personService
{
    private:
        personRepository personRepo;
        computerRepository compRepo;
        linkRepository linkRepo;

    public:
        personService();
        std::string trim(std::string str);
        std::vector<Person> getScientistData(int sortChoice);
        std::vector<Person> searchPerson(std::string str);
        std::vector<Person> getTableRelation(int computerID);
        std::vector<Person> searchPersonBirthYear(std::string year);
        std::vector<Person> searchPersonDeathYear(std::string year);
        std::vector<Computers> getComputerData(int sortChoice2);
        std::vector<Computers> getTableRelationC(int scientistID);
        std::vector<Computers> searchComputerBuildYear(std::string str);
        std::vector<Computers> searchComputer(std::string str);
        void addPerson(Person p);
        void addComputer(Computers c);
        void addLink(Linker l);
};


#endif // PERSONSERVICE_H


