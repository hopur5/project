#include "personservice.h"

personService::personService()
{
    personRepo = personRepository();
}

void personService::addPerson(Person p)// Adds a person to the database
{
    personRepo.add(p);
}

void personService::addComputer(Computers c)// Adds a computer to the database
{
    compRepo.addComputer(c);
}

void personService::addLink(Linker l)// Adds a link to the database
{
    linkRepo.addLink(l);
}

std::vector<Person> personService::getTableRelation(int computerID)// // Gets all the scientists that worked on/are linked to this computer ID and puts them into a vector
{
    std::vector<Person> list = personRepo.getTableRelation(computerID);

    return list;
}

std::vector<Computers> personService::getTableRelationC(int scientistID)// Gets all the computers linked to/worked on by this scientist ID and puts them into a vector
{
    std::vector<Computers> list = compRepo.getTableRelation(scientistID);

    return list;
}

std::vector<Person> personService::getScientistData(int sortChoice)// Gets the sorted scientists from the database and puts them into a vector
{
    std::vector<Person> s = personRepo.scientistsFromDatabase(sortChoice);

    return s;
}

std::vector<Computers> personService::getComputerData(int sortChoice2)// Gets the sorted computers from the database and puts them into a vector
{    
    std::vector<Computers> s = compRepo.computersFromDatabase(sortChoice2);

    return s;
}


std::string personService::trim(std::string str)// Deletes all the white space of a string
{
    const std::string whitespace = " \t\f\v\n\r";
    int start = str.find_first_not_of(whitespace);
    int end = str.find_last_not_of(whitespace);
    str.erase(0,start);
    str.erase((end - start) + 1);

    return str;
}

std::vector<Person> personService::searchPerson(std::string str)// Searches a person from the database by name and puts it into a vector
{
    std::vector<Person> list;
    list = personRepo.searchPerson(str);

    return list;
}

std::vector<Computers> personService::searchComputer(std::string str)// Searches a computer from the database by name and puts it into a vector
{
    std::vector<Computers> list;
    list = compRepo.searchComputer(str);

    return list;
}

std::vector<Person> personService::searchPersonBirthYear(std::string year)// Searches a scientists from the database by birth year and puts it into a vector
{
    std::vector<Person> list;
    list = personRepo.searchPersonBirthYear(year);

    return list;
}
std::vector<Person> personService::searchPersonDeathYear(std::string year)// Searches a scientists from the database by death year and puts it into a vector
{
    std::vector<Person> list;
    list = personRepo.searchPersonDeathYear(year);

    return list;
}

std::vector<Computers> personService::searchComputerBuildYear(std::string str)// Searches a computer from the database by build year and puts it into a vector
{
    std::vector<Computers> list;
    list = compRepo.searchComputerBuildYear(str);

    return list;
}

